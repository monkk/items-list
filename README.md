# Description

Vue.js project demonstrating list of products fetched from specific backend.   
Following features were implemented: sorting, paginating, selecting number of products to show on the page.  
Sorting is done with the help of list animations in Vue.js.  

# Comments on lazyload:
Lazyload is implemented with the help of third-pary library (Gzipped size: 2.1kB): [lazyload.js](https://github.com/verlok/lazyload)    
Though as both lazyload and sorting across whole list are implemented, there is a small timeframe in which images swapped to the viewport are shown as blank.  
That's a feature, because otherwise I should prefetch images beforehand, which results in extra traffic.
There is also a posibility to prefetch extra-small versions of images (like 5x5 pixels) and blur them, but unfortunately I couldn't achieve that with current backend. 
Size was way to big ~ 1kB for 2x2 pixel photo.

# Development tools

As a fan of prettier I decided to include it here as well as it tremendeously improves code readability.  
I decided against any CSS processors as things are pretty simple here.

# Demo

Built version can be found here: https://monkk.ru/stuff/ 


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
