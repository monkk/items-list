import Vue from 'vue';
import App from './App.vue';
import axios from 'axios';
import LazyLoad from 'vanilla-lazyload';

Vue.prototype.$http = axios;
Vue.prototype.$lazyLoad = new LazyLoad();

Vue.config.productionTip = false;

Vue.filter('price', function(value) {
  if (!value) return '';
  return `${(value / 100).toFixed(2)} €`;
});

new Vue({
  render: h => h(App)
}).$mount('#app');
